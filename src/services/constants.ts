const API_KEY = `16e14e9af53c31cb98f8410d529883f4`;
const URL_SEARCH = 'https://api.themoviedb.org/3/search/movie';
const URL_POPULAR = 'https://api.themoviedb.org/3/movie/popular';
const URL_TOP_RATED = 'https://api.themoviedb.org/3/movie/top_rated';
const URL_UPCOMING = 'https://api.themoviedb.org/3/movie/upcoming';

export { API_KEY, URL_SEARCH, URL_POPULAR, URL_TOP_RATED, URL_UPCOMING };
