import {
    API_KEY,
    URL_SEARCH,
    URL_POPULAR,
    URL_TOP_RATED,
    URL_UPCOMING,
} from './constants';

export default class MovieApiService {
    _query: string;
    page: number;
    _currentLink: string;

    constructor() {
        this._query = '';
        this.page = 1;
        this._currentLink = '';
    }

    async fetchPopularMovies(): Promise<void> {
        const url = `${URL_POPULAR}?api_key=${API_KEY}&language=en-US&page=${this.page}&include_adult=false`;
        this.currentLink = url;
        try {
            const fetchMovie = await fetch(url);

            return await fetchMovie.json();
        } catch (err) {
            console.log(err);
        }
    }

    async fetchTopRatedMovies(): Promise<void> {
        const url = `${URL_TOP_RATED}?api_key=${API_KEY}&language=en-US&page=${this.page}&include_adult=false`;
        this.currentLink = url;
        try {
            const fetchMovie = await fetch(url);
            return await fetchMovie.json();
        } catch (err) {
            console.log(err);
        }
    }

    async fetchUpcomingMovies(): Promise<void> {
        const url = `${URL_UPCOMING}?api_key=${API_KEY}&language=en-US&page=${this.page}&include_adult=false`;
        this.currentLink = url;
        try {
            const fetchMovie = await fetch(url);
            return await fetchMovie.json();
        } catch (err) {
            console.log(err);
        }
    }

    async fetchSearchMovies(): Promise<void> {
        const url = `${URL_SEARCH}?api_key=${API_KEY}&language=en-US&query=${this._query}&page=${this.page}&include_adult=false`;
        this.currentLink = url;
        try {
            const fetchMovie = await fetch(url);
            return await fetchMovie.json();
        } catch (err) {
            console.log(err);
        }
    }

    incrementPage(): void {
        this.page += 1;
    }
    resetPage(): void {
        this.page = 1;
    }

    async loadMore(): Promise<void> {
        this.incrementPage();
        const url = `${this._currentLink}?api_key=${API_KEY}&language=en-US&page=${this.page}&include_adult=false`;

        try {
            const fetchMovie = await fetch(url);
            return await fetchMovie.json();
        } catch (err) {
            console.log(err);
        }
    }

    get currentLink(): string {
        return this._currentLink;
    }

    set currentLink(newCurrentLink: string) {
        this._currentLink = newCurrentLink;
    }

    get query(): string {
        return this._query;
    }

    set query(newSearchName: string) {
        this._query = newSearchName;
    }
}
