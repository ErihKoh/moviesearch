const refs = {
    searchForm: document.querySelector('.form-inline') as Element,
    movieGallery: document.querySelector('#film-container') as Element,
    btnPopular: document.querySelector('#popular'),
    btnUpcoming: document.querySelector('#upcoming'),
    btnTopRated: document.querySelector('#top_rated'),
    btnHeart: document.querySelector('.bi'),
    randomMovie: document.querySelector('#random-movie') as Element,
    btnLoadMore: document.querySelector('#load-more') as Element,
};

export default refs;
