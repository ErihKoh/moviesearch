import MovieApiService from './services/api-service';
import refs from './services/refs';
import markup from './components/markup';
import { dataMapper } from './helpers/dataMapper';

const movieApiService = new MovieApiService();

export async function render(): Promise<void> {
    // TODO render your app here

    popularMovies();

    refs.btnPopular?.addEventListener('click', popularMovies);
    refs.btnUpcoming?.addEventListener('click', upcomingMovies);
    refs.btnTopRated?.addEventListener('click', topRatedMovies);
    refs.btnLoadMore?.addEventListener('click', loadMore);
    refs.searchForm?.addEventListener('submit', searchMovies);

    async function popularMovies(): Promise<void> {
        clearMovieMarkup();
        movieApiService.resetPage();
        const { results }: any = await movieApiService.fetchPopularMovies();
        const movies = dataMapper(results);
        movies.map((el: any) => {
            refs.movieGallery.insertAdjacentHTML(
                'beforeend',
                markup.createMarkup(el)
            );
        });
        randomMovie(results);
    }

    async function upcomingMovies(): Promise<void> {
        clearMovieMarkup();
        movieApiService.resetPage();
        const { results }: any = await movieApiService.fetchUpcomingMovies();
        const movies = dataMapper(results);
        movies.map((el: any) => {
            refs.movieGallery.insertAdjacentHTML(
                'beforeend',
                markup.createMarkup(el)
            );
        });
        randomMovie(results);
    }

    async function topRatedMovies(): Promise<void> {
        clearMovieMarkup();
        movieApiService.resetPage();
        const { results }: any = await movieApiService.fetchTopRatedMovies();
        const movies = dataMapper(results);
        movies.map((el: any) => {
            refs.movieGallery.insertAdjacentHTML(
                'beforeend',
                markup.createMarkup(el)
            );
        });
        randomMovie(results);
    }

    async function searchMovies(e: any): Promise<void> {
        e.preventDefault();
        movieApiService.query = e.currentTarget.elements.search.value;
        if (movieApiService._query === '') {
            return;
        }

        clearMovieMarkup();
        movieApiService.resetPage();
        const { results }: any = await movieApiService.fetchSearchMovies();
        const movies = dataMapper(results);
        movies.map((el: any) => {
            refs.movieGallery.insertAdjacentHTML(
                'beforeend',
                markup.createMarkup(el)
            );
        });
        randomMovie(results);
    }

    function clearMovieMarkup() {
        refs.movieGallery.innerHTML = '';
    }

    function randomMovie(movies: any): void {
        const item: any = movies[Math.floor(Math.random() * movies.length)];
        refs.randomMovie.innerHTML = markup.createMarkupBanner(item);
    }

    async function loadMore(): Promise<void> {
        const movePage = document.body.scrollHeight;
        const { results }: any = await movieApiService.loadMore();
        results.map((el: any) => {
            refs.movieGallery.insertAdjacentHTML(
                'beforeend',
                markup.createMarkup(el)
            );
        });
        window.scrollTo({
            top: movePage,
            behavior: 'smooth',
        });
    }
}
