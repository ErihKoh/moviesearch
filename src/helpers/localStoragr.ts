export const setMovie = (movie: any): void => {
    const moviesId = localStorage.getItem('favoriteMovie') as string;
    const moviesArrId = JSON.parse(moviesId) as [];
    const isExistId = moviesArrId.find((id: string) => id === movie.id);
    const newArr = moviesArrId.filter((id: string) => id !== movie.id);

    if (!isExistId) {
        localStorage.setItem('favoriteMovie', JSON.stringify([...movie.id]));
        return;
    } else {
        localStorage.setItem('favoriteMovie', JSON.stringify(newArr));
    }
};
