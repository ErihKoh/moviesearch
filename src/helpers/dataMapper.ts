export const dataMapper = (results: []): any => {
    const movies = results.map(
        ({ poster_path, overview, release_date, title }: any) => ({
            poster_path,
            overview,
            title,
            release_date,
        })
    );

    return movies;
};
